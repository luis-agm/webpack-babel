import Home from "./pages/Home"
import React from "react"
import ReactDOM from "react-dom"
import './styles.scss'

global.isClient = typeof window !== undefined

const wrapper = document.getElementById( "app" )
ReactDOM.render( <Home />, wrapper )
