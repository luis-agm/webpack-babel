import React, { useState } from "react"
import Input from "components/Input"

if ( global.isClient ) require( "./styles.scss" )

const FormContainer = ( props ) => {
    const [ greeting, setGreeting ] = useState( '' )

    const handleChange = ( evt ) => {
      const greet = evt.target.value
      setGreeting( greet )
    }

    const onSubmit = ( evt ) => {
      evt.preventDefault()
      console.log( greeting )
      alert( `Well, ${greeting} to you too!` )
    }

    return (
      <form id='greeting-form' className='super-form' onSubmit={onSubmit}>
        <Input
          text='Greeting'
          label='greeting'
          type='text'
          id='greeting-input'
          value={greeting}
          onChange={handleChange}
        />
      </form>
    )
  }


export default FormContainer
