import React from "react"

const Input = ( { label, text, type, id, value, onChange } ) => (
  <div className='form-group'>
    <label htmlFor={label}>{text}</label>
    <input
      type={type}
      className='form-control'
      id={id}
      value={value}
      onChange={onChange}
      required
    />
  </div>
)

export default Input
