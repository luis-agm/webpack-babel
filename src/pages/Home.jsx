import React from 'react'
import Form from '../components/Form'

function Home() {
  return (
    <div className='home__container'>
      <Form />
    </div>
  )
}

export default Home
