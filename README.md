## React boilerplate using Webpack, Babel, ESlint, Sass

Wanted to make something useful without `Create-react-app`.

For development, `webpack-dev-server` is used, and `webpack` as a bundler for production.

ESLint with a custom config extending from `standard` is used for linting, although it is recommended to manually format with `prettier`. Prettier is able to coexist with ESLint with a few tweaks, but ESlint rules such as `space-in-parens` conflict with prettier, so it is not included in this boilerplate at least for now.

No UI library is included, since that choice really varies from app to app. However webpack is set up to process `SCSS` files.